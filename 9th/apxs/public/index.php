<?php ini_set('display_errors', 1); ?>
<?php
$id = isset($_GET['id']) ? $_GET['id'] : 1;
$id = preg_replace('/\s+/', '', $id);

try {
    $pdo = new PDO('mysql:host=cookbook_mariadb;dbname=recipes;charset=utf8', 'cookbook', 'LittleChef',
		   array(PDO::ATTR_EMULATE_PREPARES => false));
} catch (PDOException $e) {
    exit('[FATAL] '.$e->getMessage());
}
$stmt = $pdo->query("SELECT * FROM notebook WHERE id=".$id);
if ($stmt) {
    $article = $stmt->fetch(PDO::FETCH_ASSOC);
    if ($article == false) {
	header("Location: /?id=1");
	exit();
    }
}
?>
<!DOCTYPE HTML>
<html>
    
    <head>
	<title>Julia's Cookbook</title>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<!--[if lte IE 8]><script src="assets/js/ie/html5shiv.js"></script><![endif]-->
	<link rel="stylesheet" href="assets/css/main.css" />
	<!--[if lte IE 9]><link rel="stylesheet" href="assets/css/ie9.css" /><![endif]-->
    </head>
    
    <body>
	<section id="banner">
	    <h2>Julia's Cookbook</h2>
	    <p>Everyday's Cooking Inspiration</p>
	</section>
	<section id="one" class="wrapper special">
	    <div class="inner">
		<header class="major">
		    <h2><?php echo htmlspecialchars($article['title']); ?></h2>
		</header>
		<p>Prep:<?php echo htmlspecialchars($article['prep']); ?>m / Cook:<?php echo htmlspecialchars($article['cook']); ?>m / Ready in:<?php echo htmlspecialchars($article['ready']); ?>m</p>
	    </div>
	</section>
	<section id="two" class="wrapper">
	    <?php include("recipes/".$article['article']); ?>
	</section>
	<section id="three" class="wrapper special">
	    <button onclick="f('http://192.168.205.1:4100/?id=<?php echo htmlspecialchars($id); ?>')">SHARE!</button>
	</section>
	
	<!-- Scripts -->
	<script src="assets/js/jquery.min.js"></script>
	<script src="assets/js/skel.min.js"></script>
	<script src="assets/js/util.js"></script>
	<!--[if lte IE 8]><script src="assets/js/ie/respond.min.js"></script><![endif]-->
	<script src="assets/js/main.js"></script>

	<script>
	 function f(string){
	     var temp = document.createElement('div');
	     temp.appendChild(document.createElement('pre')).textContent = string;
	     var s = temp.style;
	     s.position = 'fixed';
	     s.left = '-100%';
	     document.body.appendChild(temp);
	     document.getSelection().selectAllChildren(temp);
	     var result = document.execCommand('copy');
	     document.body.removeChild(temp);
	     if (result) {
		 alert("The link of this page is copied to the clipboard.");
	     }
	 }
	</script>
    </body>
</html>
