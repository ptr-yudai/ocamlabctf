/*
  $ gcc login.c -o login -fno-stack-protector -fno-PIE -no-pie
 */
#include <stdio.h>
#include <string.h>

char flag[128] = "FLAG{     DUMMY     }";

void read_flag(void)
{
  FILE *fp;
  fp = fopen("flag", "r");
  fread(flag, 1, 127, fp);
  fclose(fp);
}

void show_flag(void)
{
  puts(":::");
  printf(":::  %s\n", flag);
  puts(":::");
}

unsigned long hash(unsigned char *str)
{
    unsigned long hash = 5381;
    int c;
    while (c = *str++)
        hash = ((hash << 5) + hash) + c;
    return hash;
}

void login(void)
{
  unsigned char user[16], pass[16];
  unsigned long huser, hpass;

  printf("Username: ");
  scanf("%s", user);
  printf("Password: ");
  scanf("%s", pass);

  huser = hash(user);
  hpass = hash(pass);
  
  if (huser == 0xf12fc8eUL && hpass == 0x29396e0aUL) {
    read_flag();
    show_flag();
  } else {
    puts("Try again.");
  }
}

int main(void)
{
  setbuf(stdout, NULL);
  puts("+-----------------------+");
  puts("| O C a m L A B . i n c |");
  puts("+-----------------------+");
  login();
}
