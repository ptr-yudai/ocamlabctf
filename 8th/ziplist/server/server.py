#!/usr/bin/env python
from flask import Flask, request, make_response, jsonify
import commands
import os
import string
import random
import base64

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 1 * 1024 * 1024

@app.route('/', methods=['POST'])
def upload():
    if 'upload' not in request.files:
        return make_response(jsonify({
            'result': 'No file is uploaded.'
        }))
    zipfile = request.files['upload']
    filename = zipfile.filename
    if filename == '':
        return make_response(jsonify({
            'result': 'Filename is empty.'
        }))
    upload_filename = "./upload/" + ''.join(
        [random.choice(string.ascii_letters) for i in range(16)]
    )
    zipfile.save(upload_filename)
    result = commands.getoutput("./ziplist " + upload_filename)
    print(type(result))
    print(result)
    print(repr(result))
    return make_response(jsonify({
        'result': base64.b64encode(result)
    }))

if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=False, port=4214)

