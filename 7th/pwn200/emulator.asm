bits 32
global main
section .data
error: db 'Invalid opecode', 10
	
section .text
main:
	call emulate
	xor ebx, ebx
exit:
	xor eax, eax
	inc eax
	int 0x80

emulate:
	mov ebp, esp
	sub esp, 0x3e8
	;; input
	xor eax, eax
	xor ebx, ebx
	xor edx, edx
	mov al, 0x3
	mov ecx, esp
	mov dx, 0x3e8
	int 0x80
	;; emulate
	xor ebx, ebx
	xor ecx, ecx
	xor edx, edx
loop:
	mov al, [esp]
	mov dl, al
	shr al, 4
	and dl, 0b1111
	call sandbox
	inc esp
	cmp esp, ebp
	jnz loop
	;; end
	ret

sandbox:
	cmp al, 0b0000
	jz i0000
	cmp al, 0b0001
	jz i0001
	cmp al, 0b0010
	jz i0010
	cmp al, 0b0011
	jz i0011
	cmp al, 0b0100
	jz i0100
	cmp al, 0b1000
	jz i1000
	cmp al, 0b1100
	jz i1100
	cmp al, 0b1111
	jz i1111
	xor eax, eax
	mov al, 0x4
	xor ebx, ebx
	inc ebx
	mov ecx, error
	xor edx, edx
	mov dl, 0x10
	int 0x80
	jmp exit
i0000:
	ret

i0001:
	add ebx, edx
	ret
i0010:
	sub ebx, edx
	ret
i0011:
	shl ebx, 1
	ret
i0100:
	add ecx, edx
	ret
i1000:
	sub ecx, edx
	ret
i1100:
	shl ecx, 1
	ret
i1111:
	mov [ebx], ecx
	ret
