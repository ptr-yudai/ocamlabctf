#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#define MAGIC_LOCAL_FILE_HEADER               0x04034B50
#define MAGIC_CENTRAL_DIRECTORY_FILE_HEADER   0x02014B50
#define MAGIC_END_OF_CENTRAL_DIRECTORY_RECORD 0x06054B50

typedef struct __attribute__((packed)) {
  uint32_t signature;
  uint16_t vertion;
  uint16_t flags;
  uint16_t compression_method;
  uint16_t modification_time;
  uint16_t modification_date;
  uint32_t crc32;
  uint32_t compressed_size;
  uint32_t uncompressed_size;
  uint16_t length_filename;
  uint16_t length_extra;
} ZipHeader;

typedef struct __attribute__((packed)) {
  uint32_t signature;
  uint16_t version;
  uint16_t flags;
  uint16_t compression_method;
  uint16_t modification_time;
  uint16_t modification_date;
  uint32_t crc32;
  uint32_t compressed_size;
  uint32_t uncompressed_size;
  uint16_t length_filename;
  uint16_t length_extra;
} LocalFileHeader;

typedef struct __attribute__((packed)) {
  uint32_t signature;
  uint16_t version;
  uint16_t version_needed;
  uint16_t flags;
  uint16_t compression_method;
  uint16_t modification_time;
  uint16_t modification_date;
  uint32_t crc32;
  uint32_t compressed_size;
  uint32_t uncompressed_size;
  uint16_t length_filename;
  uint16_t length_extra;
  uint16_t length_comment;
  uint16_t disk_start;
  uint16_t internal_attr;
  uint32_t external_attr;
  uint32_t header_offset;
  char *filename;
} CentralDirectoryFileHeader;

typedef struct __attribute__((packed)) {
  uint32_t signature;
  uint16_t disk_number;
  uint16_t disk_number_cd;
  uint16_t disk_entries;
  uint16_t total_entries;
  uint32_t cd_size;
  uint32_t cd_offset;
  uint16_t length_comment;
} EndOfCentralDirectoryRecord;

void zip_ts2time(uint16_t, int*, int*, int*);
void zip_ts2date(uint16_t, int*, int*, int*);
int zip_check_header(FILE*, EndOfCentralDirectoryRecord*, char*);
int zip_get_entries(FILE*, EndOfCentralDirectoryRecord*, CentralDirectoryFileHeader***);
