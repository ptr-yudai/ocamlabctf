#include <stdio.h>
#include <stdlib.h>

int main(void) {
  int i, j;
  unsigned int unixtime = 1542291070;
  FILE *fp, *fout;
  char bytes[4];
  fp = fopen("top_secret.xored", "r");

  fseek(fp, 0, SEEK_SET);
  fread(bytes, 1, 4, fp);

  for(i = 0; i < 1; i++) {
    srand(unixtime - i);
    printf("%d %d %d\n", rand()%0x100, rand()%0x100, rand()%0x100);
    if ((bytes[0] ^ (rand()%0x100)) == 0x50) {
      if ((bytes[1] ^ (rand()%0x100)) == 0x4b) {
	if ((bytes[2] ^ (rand()%0x100)) == 0x03) {
          if ((bytes[3] ^ (rand()%0x100)) == 0x04) {
	    printf("%d\n", unixtime);
	    unixtime = unixtime + i;
	    break;
	  }
	}
      }
    }
  }

  if (unixtime == 1542288716) {
    puts("Nothing found...");
    return 1;
  }
  
  fout = fopen("top_secret.zip", "w");
  
  fseek(fp, 0, SEEK_SET);
  srand(unixtime);
  for(i = 0; i < 313; i++) {
    char byte;
    fread(&byte, 1, 1, fp);
    byte ^= rand() % 0x100;
    fwrite(&byte, 1, 1, fout);
  }

  fclose(fp);
  fclose(fout);
}
