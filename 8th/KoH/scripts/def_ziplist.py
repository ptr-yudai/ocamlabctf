#!/usr/bin/env python
import commands

def tokens():
    cid = commands.getoutput("docker ps | grep ziplist | awk '{print $1}'")
    # Get team tokens
    result = commands.getoutput("docker exec " + cid + " cat defense")
    token_list = []
    for token in result.split('\n'):
        if token[:6] != 'token-':
            continue
        token_list.append(token[:38])
    # Sweep out
    commands.getoutput("docker exec " + cid + " cp /dev/null defense")
    # Make sure to return unique tokens
    return set(token_list)
