#/usr/bin/python3
from web3 import Web3, HTTPProvider

passphrase = "74k0y4k1b4LL07"

abi = [{"constant": False,"inputs": [],"name": "register","outputs": [],"payable": False,"stateMutability": "nonpayable","type": "function"},{"constant": True,"inputs": [{"name": "","type": "bytes32"}],"name": "votes","outputs": [{"name": "","type": "uint256"}],"payable": False,"stateMutability": "view","type": "function"},{"constant": True,"inputs": [{"name": "token","type": "bytes32"}],"name": "result","outputs": [{"name": "","type": "uint256"}],"payable": False,"stateMutability": "view","type": "function"},{"constant": True,"inputs": [],"name": "flag","outputs": [{"name": "","type": "string"}],"payable": False,"stateMutability": "view","type": "function"},{"constant": True,"inputs": [{"name": "","type": "uint256"}],"name": "tokenList","outputs": [{"name": "","type": "bytes32"}],"payable": False,"stateMutability": "view","type": "function"},{"constant": True,"inputs": [{"name": "","type": "address"}],"name": "voters","outputs": [{"name": "voted","type": "bool"},{"name": "weight","type": "uint256"}],"payable": False,"stateMutability": "view","type": "function"},{"constant": False,"inputs": [{"name": "token","type": "bytes32"}],"name": "vote","outputs": [],"payable": False,"stateMutability": "nonpayable","type": "function"},{"constant": True,"inputs": [{"name": "token","type": "bytes32"}],"name": "validToken","outputs": [{"name": "","type": "bool"}],"payable": False,"stateMutability": "view","type": "function"},{"inputs": [{"name": "teamTokens","type": "bytes32[]"},{"name": "attackFlag","type": "string"}],"payable": False,"stateMutability": "nonpayable","type": "constructor"}]
binary = open("bin").read()

w = Web3()
#w.admin.addPeer("enode://8993fb89cdcdc265a76839f437036a30a1cb97ad86a0276dc399cdf35814d968ae142c53a87bd4bfe064b3d43883063edad1889f19e493aba88eb0966e73d8dd@192.168.1.6:8545")
#w.personal.newAccount(passphrase)
print(w.eth.accounts[0])
w.personal.unlockAccount(w.eth.accounts[0], passphrase)
w.eth.defaultAccount = w.eth.accounts[0]
Ballot = w.eth.contract(abi=abi, bytecode=binary)
tx_hash = Ballot.constructor(
    ["0x344d753746537938564e6d6c7a78333076546c6b5044436a35473356626d6954", "0x58327a4d733670614b314562346b584749426661305475476773533771386c33"],
    "flag-WtfKLRsdJr7FhbWqekwxkl2OxQ6HaU5U"
).transact()
tx_receipt = w.eth.waitForTransactionReceipt(tx_hash)

ballot = w.eth.contract(
    address = tx_receipt.contractAddress,
    abi = abi
)

print(tx_receipt.contractAddress)
