#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char** argv)
{
  int i, filesize;
  int fd;
  
  if (argc < 2) {
    printf("Usage: %s [filepath]\n", argv[0]);
    return 1;
  }

  // Open
  fd = open(argv[1], O_WRONLY);
  filesize = lseek(fd, 0, SEEK_END);
  
  // Randomize
  unsigned int unixtime = time(NULL);
  printf("%d\n", unixtime);
  srand(unixtime);
  for(i = 0; i < filesize; i++) {
    char byte;
    lseek(fd, i, SEEK_SET);
    read(fd, &byte, 1);
    
    byte ^= rand() % 0x100;
    
    lseek(fd, i, SEEK_SET);
    write(fd, &byte, 1);
    
    // Sync
    // fsync(fd);
  }
  close(fd);
  return 0;

  // Delete
  if (remove(argv[1]) == 0) {
    puts("Deleted sucessfully.");
  } else {
    puts("Unable to delete the file.");
  }
  return 0;
}
