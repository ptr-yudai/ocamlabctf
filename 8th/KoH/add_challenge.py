import config
import sqlite3
import glob
import json
from contextlib import closing

with closing(sqlite3.connect(config.dbpath)) as conn:
    c = conn.cursor()
    for path in glob.glob("challenges/*.json"):
        with open(path, "r") as f:
            chall = json.load(f)
            title = chall['title']
            problem = chall['problem']
            score = chall['score']
            category = chall['category']
            flag = chall['flag']
        c.execute("SELECT * FROM challenge WHERE title=?", (title,))
        result = c.fetchone()
        if result:
            print("[DUP] Aborting: " + title)
        else:
            c.execute(
                "INSERT INTO challenge(title, problem, score, category, flag) VALUES(?, ?, ?, ?, ?)",
                (title, problem, score, category, flag)
            )
    conn.commit()
