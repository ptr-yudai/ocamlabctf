/*
  $ gcc -masm=intel emulator.c -o emulator -fstack-protector -fno-PIE -m32
 */
#include <stdio.h>
#include <stdlib.h>

#define OBJSIZE 0x1000

char *objcode;

void __attribute__((_cdecl)) sandbox(char instruction, char data)
{
  switch(instruction) {
  case 0:
    break;
  case 1:
    break;
  case 2:
    break;
  case 3:
    break;
  }
}

void emulate()
{
  int i;
  char instruction, data;
  for(i = 0; i < OBJSIZE; i++) {
    instruction = objcode[i] >> 4;
    data = objcode[i] & 0b1111;
    sandbox(instruction, data);
  }
}

int main(void)
{
  setbuf(stdout, NULL);

  objcode = (char*)malloc(sizeof(char) * OBJSIZE);
  fread(objcode, sizeof(char), OBJSIZE, stdin);
  emulate();
  
  return 0;
}
