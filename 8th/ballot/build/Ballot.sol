pragma solidity ^0.4.24;

contract Ballot {
  struct Voter {
    bool    voted;
    uint    weight;
  }
  mapping(address => Voter) public voters;
  mapping(bytes32 => uint ) public votes;
  bytes32[] public tokenList;
  string    public flag;
  
  constructor(bytes32[] teamTokens, string attackFlag) public {
    tokenList = teamTokens;
    flag = attackFlag;
  }

  function register() public {
    require(!voters[msg.sender].voted);
    voters[msg.sender].weight = 1;
  }

  function vote(bytes32 token) public {
    require(validToken(token));
    Voter storage sender = voters[msg.sender];
    require((sender.weight > 0) && !sender.voted);
    votes[token] += sender.weight;
    sender.voted = true;
  }

  function result(bytes32 token) view public returns (uint) {
    require(validToken(token));
    return votes[token];
  }

  function validToken(bytes32 token) view public returns (bool) {
    for(uint i = 0; i < tokenList.length; i++) {
      if (tokenList[i] == token) {
	return true;
      }
    }
    return false;
  }
}
