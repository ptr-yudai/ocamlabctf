# coding: utf-8
from PIL import Image

class Piet(object):
    DP_LEFT, DP_RIGHT, DP_UP, DP_BOTTOM = 0, 1, 2, 3
    CC_LEFT, CC_RIGHT = True, False
    """Piet言語
    OCamLab KoHに出題する用の簡易インタプリタです
    """
    def __init__(self, path):
        """ 初期化 """
        self.COLOR = [
            [0xFFC0C0, 0xFF0000, 0xC00000],
            [0xFFFFC0, 0xFFFF00, 0xC0C000],
            [0xC0FFC0, 0x00FF00, 0x00C000],
            [0xC0FFFF, 0x00FFFF, 0x00C0C0],
            [0xC0C0FF, 0x0000FF, 0x0000C0],
            [0xFFC0FF, 0xFF00FF, 0xC000C0]
        ]
        self.PP = [0, 0]
        self.CC = self.CC_LEFT
        self.DP = self.DP_RIGHT
        self.stack = []
        self.img = Image.open(path).convert('RGB')

    def current_color(self):
        """ PPの色を取得 """
        color = self.img.getpixel(self.PP)
        return (color[0]<<16) | (color[1]<<8) | color[2]
    
    def current_color(self):
        """ DPで進めた場合の色を取得 """
        ev = list(self.PP)
        self.move_by_dp()
        return ev

    def move_by_dp(self):
        """ DPでポインタを進める """
        if self.DP == self.DP_LEFT:
            self.PP[0] -= 1
        elif self.DP == self.DP_RIGHT:
            self.PP[0] += 1
        elif self.DP == self.DP_UP:
            self.PP[1] -= 1
        elif self.DP == self.DP_BOTTOM:
            self.PP[1] += 1

    def move_by_cc(self):
        """ CCでポインタを進める """
        if self.CC == self.CC_LEFT:
    
    def find_edge(self):
        """ カラーブロックの端へ移動 """
        pre_color = self.current_color()
        while pre_color == self.next_color():
            self.move_by_dp()

    def find_codel(self):
        """ コーデルを探索 """
        pre_color = self.current_color()
        while pre_color
        
    def step(self):
        """ 1サイクル実行 """
        self.find_edge()
        self.find_codel()
        
    def run(self):
        """ 実行 """
        self.step()
        

if __name__ == '__main__':
    piet = Piet("sample.gif")
    piet.run()
