/*
  $ gcc main.c -o passmon -fno-stack-protector -z execstack -m32
  $ strip --strip-all passmon
 */
#include <stdio.h>
#include <string.h>
#include "aes.h"

#define LENGTH 64
unsigned char flag_enc[] = {0xd6, 0xe4, 0xbc, 0x47, 0x58, 0xaa, 0x07, 0xc0, 0x8a, 0x1d, 0x53, 0x6e, 0xea, 0xa3, 0x9c, 0x48,
			    0x57, 0xc5, 0x6b, 0x27, 0xf4, 0x98, 0x92, 0xe1, 0x3f, 0x86, 0x5c, 0x00, 0xdb, 0xa5, 0x31, 0xfa,
			    0x74, 0xc5, 0x09, 0x31, 0xf9, 0x18, 0xe5, 0xf0, 0x5b, 0xf8, 0x47, 0x86, 0xca, 0x00, 0xce, 0x0b,
			    0xee, 0x47, 0xf6, 0x53, 0xd6, 0xc1, 0x58, 0xea, 0x76, 0x94, 0xb7, 0x75, 0x1f, 0x71, 0xaf, 0xf3};
unsigned char key[16];
uint8_t iv[]  = { 0xf0, 0xe1, 0xd2, 0xc3, 0xb4, 0xa5, 0x96, 0x87, 0x78, 0x69, 0x5a, 0x4b, 0x3c, 0x2d, 0x1e, 0x0f };

/*
int encrypt_aes128_cbc(unsigned char* flag)
{
  struct AES_ctx ctx;
  AES_init_ctx_iv(&ctx, key, iv);
  AES_CBC_encrypt_buffer(&ctx, flag, LENGTH);
  return 0;
}
//*/

int decrypt_aes128_cbc(void)
{
  int n, i;
  struct AES_ctx ctx;
  
  AES_init_ctx_iv(&ctx, key, iv);
  AES_CBC_decrypt_buffer(&ctx, flag_enc, LENGTH);
  
  n = flag_enc[LENGTH-1];
  if (n >= 16) {
    return 1;
  }
  for(i = 1; i <= n; i++) {
    if (flag_enc[LENGTH-n] != n) {
      return 1;
    }
  }

  return 0;
}

void unlock(void)
{
  FILE *fp;
  char flag[80];
  int result;
  // Decrypt
  fread(flag_enc, 1, LENGTH, stdin);
  result = decrypt_aes128_cbc();
  if (result == 0) {
    puts("[+] The door is unlocked.");
  } else {
    puts("[-] Try again.");
    return;
  }
  // Read the flag
  fp = fopen("/mnt/sdcard/flag", "rb");
  fread(flag, 1, 63, fp);
  fclose(fp);
  printf("[+] %s\n", flag);
  // Is that really OK?
  if (strncmp(flag_enc, "SECCON", 6) == 0) {
    fp = fopen("/mnt/sdcard/token", "a");
    printf("Team Token >> ");
    scanf("%79s", flag);
    fprintf(fp, "%s\n", flag);
    fclose(fp);
  } else {
    puts("[-] ...? Something is wrong...");
  }
}

void diagnose(void)
{
  int result;
  puts("[+] Checking Memory Corruption...");
  result = decrypt_aes128_cbc();
  if (result == 0) {
    puts("[+] Correct.");
  } else {
    puts("[-] Memory Corruption Detected!");
    puts("[-] Report to the admin and fix this device.");
  }
}

int main()
{
  int n;
  FILE *fp;
  setbuf(stdout, NULL);
  
  // Read the key
  fp = fopen("/mnt/sdcard/keyfile", "rb");
  fread(key, 1, 16, fp);
  fclose(fp);
  
  /*
  int i;
  unsigned char flag[] = "SECCON{ATTACK_on_the_AES_of_the_CBC_in_a_DEVICE_for_a_FUN}\x06\x06\x06\x06\x06\x06";
  encrypt_aes128_cbc(flag);
  for(i = 0; i < LENGTH; i++) {
    printf("0x%02x, ", flag[i]);
  }
  return 0;
  //*/

  puts("=========================");
  puts("[1] Unlock the door      ");
  puts("[2] Run self-diagnosis   ");
  puts("=========================");
  scanf("%d", &n);
  getc(stdin);
  if (n == 1) {
    unlock();
  } else if (n == 2) {
    diagnose();
  }
  
  return 0;
}
