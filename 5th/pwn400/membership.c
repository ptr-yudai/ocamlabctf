/*
  $ gcc membership.c -o membership -fno-PIE -no-pie -m32
 */
#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define MAX_NUM 4

typedef struct {
  unsigned int id;
  unsigned char age;
  char* name;
} ID;

void show_flag(void)
{
  FILE *fp;
  char flag[64];
  fp = fopen("flag", "r");
  fread(flag, 1, 63, fp);
  fclose(fp);
  puts(flag);
}

void issue(int n)
{
  ID *cards[MAX_NUM];
  int i;
  puts("Creating new membership cards...");
  for(i = 0; i < n; i++) {
    cards[i] = (ID*)malloc(sizeof(ID));
    cards[i]->id = (unsigned int)rand() % 100000;
    cards[i]->age = 0;
    cards[i]->name = (char*)malloc(16);
    if (cards[i]->name == NULL) {
      puts("Failed in creating a card.");
      puts("We are sorry for the inconvenience.");
      puts("Please take this flag instead of the card.");
      show_flag();
      exit(1);
    }
  }
  sleep(3);
  puts("OK. Enter owner's information.");
  for(i = 0; i < n; i++) {
    printf("# CARD %d\n", i + 1);
    printf("Age: ");
    scanf("%hhd", &(cards[i]->age));
    printf("Name: ");
    scanf("%s", cards[i]->name);
  }
  puts("Just a moment...");
  sleep(3);
  puts("OK. Here are your new membership cards.");
  for(i = 0; i < n; i++) {
    puts("\x1b[47m                        \x1b[49m");
    puts("\x1b[47m\x1b[31m  Ocamlab's Membership  \x1b[39m\x1b[49m");
    puts("\x1b[47m                        \x1b[49m");
    printf("\x1b[47m\x1b[30m ID  : %05d            \x1b[39m\x1b[49m\n", cards[i]->id);
    printf("\x1b[47m\x1b[30m Age : %02d               \x1b[39m\x1b[49m\n", cards[i]->age);
    printf("\x1b[47m\x1b[30m Name: %16s \x1b[39m\x1b[49m\n", cards[i]->name);
    puts("\x1b[47m                        \x1b[49m");
    puts("");
  }
}

int getnum(void)
{
  int n;
  scanf("%d", &n);
  return n;
}

int main(void)
{
  int n;
  setbuf(stdout, NULL);
  srand(time(NULL));
  puts("+-------------------------+");
  puts("| OCamLAB's Membership ID |");
  puts("+-------------------------+");
  printf("How many cards would you like to issue?\n >> ");
  n = getnum();
  if (n <= 0) {
    puts("Please enter a positive number.");
    exit(1);
  }
  if (n > MAX_NUM) {
    printf("You cannot issue more than %d cards at once.\n", MAX_NUM);
    exit(1);
  }
  issue(n);
  return 0;
}
