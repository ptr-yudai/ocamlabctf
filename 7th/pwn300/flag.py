flag = "guaTZyDbSqln0kNcxKUr7ettJXSERlEk"
a = []
for i in range(8):
    a.append(flag[i*4:(i+1)*4])
q = map(lambda x: int(x.encode("hex"), 16), a)

for p in q:
    a = p & 0xFF
    b = (p >> 8) & 0xFF
    c = (p >> 16) & 0xFF
    d = p >> 24
    x = ((0xFF^a) << 24) + ((0xFF^b) << 16) + ((0xFF^c) << 8) + (0xFF^d)
    print x
