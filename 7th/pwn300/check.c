/*
  $ gcc check.c -o check
 */
#include <stdio.h>

unsigned int code[] = {2879294104, 2646312613, 2442366636, 2628883663, 2376774791, 2341182152, 3131877301, 2495255469};

void show_flag()
{
  int i;
  char flag[33];
  char *ptr = flag;
  for(i = 0; i < 8; i++) {
    unsigned char d = (code[i] & 0xFF) ^ 0xFF;
    unsigned char c = ((code[i] >> 8) & 0xFF) ^ 0xFF;
    unsigned char b = ((code[i] >> 16) & 0xFF) ^ 0xFF;
    unsigned char a = (code[i] >> 24) ^ 0xFF;
    ptr[0] = d;
    ptr[1] = c;
    ptr[2] = b;
    ptr[3] = a;
    ptr += 4;
  }
  flag[32] = 0;
  printf("flag-%s\n", flag);
}

void decrypt(char *password)
{
  char *start = (char*)show_flag;
  char *end   = (char*)decrypt;
  char *ptr;
  int i;
  for(ptr = start; ptr < end; ptr++) {
    *ptr ^= password[i];
    i++;
    if (password[i] == 0) {
      i = 0;
    }
  }
}

int main(void)
{
  char password[16];
  printf("Password: ");
  scanf("%15s", password);
  decrypt(password);
  show_flag();
}
