#!/usr/bin/env python
from flask import Flask, request, make_response, jsonify
import commands
import os
import string
import random

app = Flask(__name__)
app.config['MAX_CONTENT_LENGTH'] = 1 * 1024 * 1024

@app.route('/', methods=['POST'])
def upload():
    if 'upload' not in request.files:
        return make_response(jsonify({
            'result': 'No file is uploaded.'
        }))
    zipfile = request.files['upload']
    filename = zipfile.filename
    if filename == '':
        return make_response(jsonify({
            'result': 'Filename is empty.'
        }))
    upload_filename = "./upload/" + ''.join(
        [random.choice(string.ascii_letters) for i in range(16)]
    )
    zipfile.save(upload_filename)
    result = commands.getoutput("./ziplist " + upload_filename)
    return make_response(jsonify({
        'result': result
    }))

if __name__ == '__main__':
    app.run(debug=False, port=4214)
