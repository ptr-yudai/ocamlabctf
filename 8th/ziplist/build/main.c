#include "zip.h"

#define FLAG_LEN 37
#define TOKEN_LEN 38

FILE *fp;

void flag(int key1, int key2)
{
  FILE *fp_flag;
  char flag[FLAG_LEN + 1];
  if (key1 == 0xDEADBEEF) {
    if (key2 == 0xCAFEBABE) {
      // Write flag
      fp_flag = fopen("defense", "a");
      if (fp_flag == NULL) {
	printf("[-] Report this bug to the admin.\r\n");
	exit(2000);
      }
      fseek(fp, 0, SEEK_SET);
      fread(flag, 1, TOKEN_LEN, fp);
      fprintf(fp_flag, "%s\n", flag);
      fclose(fp_flag);
      puts("\nYour token is successfully written.\n");
      exit(2019);
    }
  }
  // Read flag
  fp_flag = fopen("attack", "r");
  if (fp_flag == NULL) {
    printf("[-] Report this bug to the admin.\r\n");
    exit(1998);
  }
  fread(flag, 1, FLAG_LEN, fp_flag);
  printf("\nCongratulations! The flag is %s\n\n", flag);
  fclose(fp_flag);
  exit(2018);
}

int main(int argc, char** argv)
{
  int i;
  int total;
  int Y, M, D, h, m, s;
  EndOfCentralDirectoryRecord footer;
  CentralDirectoryFileHeader **entry;
  char comment[64];
  
  if (argc < 2) {
    printf("Usage: %s [zip]\n", argv[0]);
    return 1;
  }

  // Open a file
  fp = fopen(argv[1], "r");
  if (fp == NULL) {
    perror(argv[1]);
    return 1;
  }

  // Check zip format
  if (zip_check_header(fp, &footer, comment)) {
    puts("Invalid zip format.");
    return 1;
  }

  printf("Archive:  %s\n", argv[1]);
  printf("Comment:  %s\n", comment);
  puts("  Length      Date    Time    Name");
  puts("---------  ---------- -----   ----");

  // Get file list
  if (zip_get_entries(fp, &footer, &entry)) {
    puts("Broken zip file.");
    return 1;
  }

  // Show the file list
  for(total = 0, i = 0; i < footer.total_entries; i++) {
    zip_ts2date(entry[i]->modification_date, &Y, &M, &D);
    zip_ts2time(entry[i]->modification_time, &h, &m, &s);
    printf("%9d  %02d-%02d-%04d %02d:%02d   %s\n",
	   entry[i]->uncompressed_size, M, D, Y, h, m, entry[i]->filename);
    total += entry[i]->uncompressed_size;
  }

  puts("---------                     -------");
  printf("%9d%21c%d files\n", total, ' ', i);

  return 0;
}
