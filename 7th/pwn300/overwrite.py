import sys
from elftools.elf.elffile import ELFFile
from elftools.elf.descriptions import describe_p_type

password = "sh0w-y0urs3lf"
start = 0x000000000040058d
end = 0x0000000000400659

name = sys.argv[1]
with open(name, "rb") as f:
    elf = ELFFile(f)

    rwe_offsets = []
    relro_offsets = []
    for i in range(elf['e_phnum']):
        program_offset = elf['e_phoff'] + i * elf['e_phentsize']
        f.seek(program_offset)
        program_header = elf.structs.Elf_Phdr.parse_stream(f)

        if program_header['p_type'] == "PT_LOAD":
            rwe_offsets.append(program_offset)
        if program_header['p_type'] == "PT_GNU_RELRO":
            relro_offsets.append(program_offset)

    f.seek(0)
    b = list(f.read())

    # Zap RELRO
    pt_null = 0
    for off in relro_offsets:
        b[off] = chr(pt_null)

    # Fix permissions
    p_flags_offset = 4
    for off in rwe_offsets:
        b[off + p_flags_offset] = chr(0x7) # PF_X|PF_W|PF_R

#for i in xrange(len(b)):
#    if b[i:i+5] == map(chr, [0xe8, 0x8d, 0x05, 0x40, 0x00]):
#        b[i + 1] = chr(0x60)
#        break

for i in xrange(len(b)):
    if b[i:i+8] == map(chr, [0x55, 0x48, 0x89, 0xe5, 0x48, 0x83, 0xec, 0x40]):
        offset = i
        break

j = 0
print end - start
for i in xrange(offset, offset + end - start):
    b[i] = chr(ord(b[i]) ^ ord(password[j]))
    j = (j + 1) % len(password)
        
s = ''.join(b)

with open(name, "wb") as f:
    f.write(s)
