bits 32
global main

section .text
main:
	call kitten
	xor ebx, ebx
exit:
	xor eax, eax
	inc eax
	int 0x80

kitten:
	mov bp, 0x1000
	mov edi, 0xCAFEBABE
	push edi
	sub sp, bp
	xor eax, eax
	xor ebx, ebx
	mov al, 0x03
	mov ecx, esp
	xor edx, edx
	mov dx, 0x2000
	int 0x80
	add sp, bp
	pop esi
	cmp esi, edi
	jnz __stack_check_fail
	ret

__stack_check_fail:
	xor ebx, ebx
	inc ebx
	jmp exit
