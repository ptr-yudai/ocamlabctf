/*
  $ gcc qr.c -o qrcode -fno-stack-protector -fno-PIE -no-pie -m32
  $ strip --strip-all ./qrcode
 */
#include <stdio.h>
#include <string.h>
#include <unistd.h>

void genQR(void)
{
  char param[] = "--error-correction=M";
  char input[100];
  
  setbuf(stdout, NULL);
  printf("\nError Correction Level(L/M/Q/H): ");
  scanf("%8s", param + 19);
  printf(param + 19);
  printf("\nText: ");
  scanf("%s", input);
  execl("/usr/bin/qr", "/usr/bin/qr", param, input, NULL);
}

int main(void)
{
  puts(" ██████╗ ██████╗     ███████╗███╗   ██╗ ██████╗ ██████╗ ██████╗ ███████╗██████╗ ");
  puts("██╔═══██╗██╔══██╗    ██╔════╝████╗  ██║██╔════╝██╔═══██╗██╔══██╗██╔════╝██╔══██╗");
  puts("██║   ██║██████╔╝    █████╗  ██╔██╗ ██║██║     ██║   ██║██║  ██║█████╗  ██████╔╝");
  puts("██║▄▄ ██║██╔══██╗    ██╔══╝  ██║╚██╗██║██║     ██║   ██║██║  ██║██╔══╝  ██╔══██╗");
  puts("╚██████╔╝██║  ██║    ███████╗██║ ╚████║╚██████╗╚██████╔╝██████╔╝███████╗██║  ██║");
  puts(" ╚══▀▀═╝ ╚═╝  ╚═╝    ╚══════╝╚═╝  ╚═══╝ ╚═════╝ ╚═════╝ ╚═════╝ ╚══════╝╚═╝  ╚═╝");
  genQR();
  return 0;
}
